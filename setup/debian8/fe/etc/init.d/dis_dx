#!/bin/sh
#
# Auto-generated script - do not edit!
#
#
# Dolphin ICS - dis_dx driver
#
### BEGIN INIT INFO
# Provides: dis_dx
# Required-Start: dis_kosif
# Required-Stop:
# Default-Start: 2 3 4 5
# Default-Stop: 0 6
# Short-Description: Start dis_dx driver
# Description: The dis_dx driver provides kernel services for remote shared-memory 
#              access, remote interrupts and more features for low-latency,
#              high-bandwidth cluster communication on user and kernel level.
#              using a Dolphin interconnect.
### END INIT INFO
# chkconfig: 35 07 20
# description: Start dis_dx driver

VERSION_TYPE=`uname -r`
BASEDIR=/opt/DIS/sbin/..

SERVICE_NAME=dis_dx
LOGFILE="/var/log/dis_dx.log"

#
# $Id: _common_init 27764 2009-08-28 12:09:22Z joachim $
#
# Copyright (C) 1993-2007 Dolphin Interconnect Solutions AS
#
# Common code for scripts to be created in init.d
#

# Make sure LD_PRELOAD is disabled before doing things with the driver etc
LD_PRELOAD=""

# paranoid!?
PATH=$PATH:/sbin

HAVE_LSB=""
# if possible, source init functions as defined by LSB
if [ -r /lib/lsb/init-functions ] ; then 
   . /lib/lsb/init-functions
   HAVE_LSB="yes"
fi

# return 'false' if not root. return 'true' if root, or if it could not 
# be determined.
is_root()
{        
        ID_CMD=`which id`
        [ -z "$ID_CMD" ] || return 0
        [ `$ID_CMD -u` != "0" ] || return 1
        return 0
}

# log datestamp and all arguments to $LOGFILE
log_msg()
{
        echo `date` $* >>$LOGFILE
}

# print to stdout, and log datestamp and all arguments to $LOGFILE
show_msg()
{
        echo $*
        [ "$1" = "-n" ] && shift
        echo `date` $* >>$LOGFILE
}

# wrapper for LSB function
log_warning()
{
        if [ -n "$HAVE_LSB" ] ; then
                log_warning_msg $*
        else
                echo "#* WARNING: "$*        
        fi
        log_msg $*
}

# wrapper for LSB function
log_failure()
{
        if [ -n "$HAVE_LSB" ] ; then
                log_failure_msg $*
        else
                echo "#* ERROR: "$*
        fi
        log_msg $*
}

# wrapper for LSB function
log_success()
{
        if [ -n "$HAVE_LSB" ] ; then
                log_success_msg $*
        else
                echo $*
        fi
        log_msg $*
}

kill_proc()
{
        if [ -n "$HAVE_LSB" ] ; then
                killproc $1
                return $?
        else
                kill_pid=`process_running $1`
                kill $kill_pid
                process_running $1 >/dev/null
                [ $? = "1" ] && return 0
                return 1
        fi
}

process_running()
{
        if [ -n "$HAVE_LSB" ] ; then
                # pidofproc() matches only the basename on some distros (like RHEL4), 
                # not the path. And it does not check if it really is the 
                # the executable, that has the name we search for. In our
                # case, this will i.e. make the init script "dis_networkmgr"
                # and the binary "dis_networkmgr" *both* match the pidofproc
                # for "dis_networkmgr", although for the script, the executable
                # really is "sh". This will make the check for "dis_networkmgr"
                # running succeed always if performed from within the init script
                # with the same name. Sigh. Need to use the work around.
                #PIDS=`pidofproc $1`
                PIDS=`ps axuw | grep $1 | grep -v grep | awk {'print $2'}`
        else
                PIDS=`ps axuw | grep $1 | grep -v grep | awk {'print $2'}`
        fi

    [ -z "$PIDS" ] && return 1
    echo $PIDS
    return 0
}


# echo the suffix of kernel modules ('ko' for kernel 2.6, 'o' otherwise)
get_mod_suffix()
{
        LINUX_REVISION=`uname -r`
        KERNEL26=`echo "$LINUX_REVISION"  | awk '/^(([^012]\.)|(2\.[^012345]))/'` 

        if [ -n "$KERNEL26" ] ; then
                echo ko
        else
                echo o
        fi
}


# determine the path to the installed modules
get_mod_dir()
{
        local modpath=$1
        local modname=$2

        if [ -r "$modpath/$modname" ] ; then
                echo $modpath
                return 0
        fi
        
        # It seems that the modules have been compiled for a different kernel
        # version! Cut of kernel version from path string and search in all 
        # available kernel version subdirectories. It's questionable if this 
        # makes sense, but anyway...
        modpath=`dirname $modpath`
        for mp in `find $modpath -type d -name \*.\*` ; do
            if [ -r "$mp/$modname" ] ; then
                echo $mp
                return 0
            fi 
        done

        # No modules-directory found        
        return 1
}


# verify if the module was built for this kernel version
check_module_version()
{
    local mod_path=$1
    local dis_service=$2

    mod_version=`modinfo $mod_path | grep vermagic: | awk {'print $2'}`
    mod_smp=`modinfo $mod_path | grep vermagic: | grep " SMP "`

    # this info is only available with kernel >2.4
    if [ -z "$mod_version" -o -z "$mod_smp" ] ; then
	return 0
    fi
    if [ `uname -r` != $mod_version ] ; then
        show_msg
        log_warning "Dolphin $dis_service: module built for kernel $mod_version"
        log_warning "Dolphin $dis_service: module running on kernel `uname -r`"
    fi
    if [ -n "`uname -v | grep ' SMP '`" -a -z "$mod_smp" ] ; then
        show_msg
        log_failure "Dolphin $dis_service: non-SMP module can not run on SMP kernel"
        return 1
    fi
    if [ -z "`uname -v | grep ' SMP '`" -a -n "$mod_smp" ] ; then
        show_msg
        log_failure "Dolphin $dis_service: SMP module can not run on non-SMP kernel"
        return 1
    fi
    
    return 0
}


# sets global MODULE_PATH
get_module_path()
{
    local mod_path=$1
    local mod_base=$2
    local cmd=$3
    local srvc=$4

    mod_name="$mod_base.`get_mod_suffix` "
    new_mod_path=`get_mod_dir $mod_path $mod_name`
    if [ $cmd != "stop" -a -z "$new_mod_path" ] ; then
        log_failure "no valid module $mod_name found in `dirname $mod_path`"
        return 1
    fi
    mod_path=$new_mod_path/$mod_name
    if [ $cmd = "start" ] ; then
        if ! check_module_version $mod_path $srvc ; then
            return 1
        fi
    fi
    
    MODULE_PATH="$mod_path"
    return 0
}


#
# perform mandatory tests
#
if ! is_root ; then
        log_failure Must be root to run this script.
        exit 4
fi
if [ "$1" != "stop" ] ; then 
        if [ -z "$BASEDIR" ]; then
                echo "ERROR: BASEDIR not set to base directory of release"
                # LSB: program not configured error
                exit 6
        fi
fi



#
# $Id: _dis_dx_init 27760 2009-08-27 10:36:10Z joachim $
#
# Copyright (C) 1993-2008 Dolphin Interconnect Solutions ASA
#
# The first part of this file containing the BASEDIR and VERSION_TYPE
# will be created of irm-install
#

# update PAT to enable write combining
program_PAT()
{
    PLATFORM=`uname -m`

    if [ $PLATFORM = "i686" -o $PLATFORM = "x86_64" ] ; then
       # Platform supporting PAT
       log_msg Platform $PLATFORM supporting PAT

       if [ -r "$MISC_CONFIG_FILE" ] ; then
            NO_PAT_PROGRAMMING=`grep "DIS_NO_PAT_UPDATE" $MISC_CONFIG_FILE`
       fi            

       if [ -z "$NO_PAT_PROGRAMMING" ] ; then
          if [ -x $BASEDIR/sbin/dis_pattool ] ; then
              log_msg updating PAT power on value
              modprobe msr 2>/dev/null

              for i in `seq 0 15`; do
                  if [ ! -d /dev/cpu/$i ]; then       
                      mkdir -p /dev/cpu/$i
                  fi
                  if [ ! -e  /dev/cpu/$i/msr ];  then  
                      mknod /dev/cpu/$i/msr c 202 $i
                  fi  
              done
             
              PATOK=`$BASEDIR/sbin/dis_pattool -p 2>&1 | grep programmed`       
              if  [ -z "$PATOK" ] ; then
                  log_warning "PAT programming failed or not needed"
              else 
                  log_msg PAT programming ok
              fi
          fi
       else
           log_msg PAT programming disabled in $MISC_CONFIG_FILE
       fi       
    fi
}


MODULE_BASE="dis_dx"
MODULE_PATH="$BASEDIR/lib/modules/$VERSION_TYPE"
# get_module_path() sets MODULE_PATH
if ! get_module_path $MODULE_PATH $MODULE_BASE $1 DX ; then
    exit 1
fi

MODULE_VERSION=`/sbin/modinfo $MODULE_PATH | grep -E '(^| )version:' | sed 's/.*version: *//g'`

MODULE_CONFIGFILE="$BASEDIR/lib/modules/dis_dx.conf"
# additional config file
MISC_CONFIG_FILE="/etc/dis/misc_config.conf"
DXDEV=/dev/DX

case $1 in
start)
    show_msg -n Starting $MODULE_VERSION

    # check if already runnig
    [ -n "`grep $MODULE_BASE /proc/devices | awk '{print $1}'`" ] && { log_success; exit 0; }

    # place commands to be executed before the driver is loaded here
    program_PAT
 
    # install the driver
    log_msg Installing DX version $MODULE_VERSION
    log_msg Reading DX driver configuration from $MODULE_CONFIGFILE

    [ -r $MODULE_CONFIGFILE ] && dxConfig=`grep -v "#" $MODULE_CONFIGFILE | uniq | tr ';' ' ' | tr -d '\n' | tr -d '\r'`
    log_msg Configuration : $dxConfig
    log_msg Loading DX driver  $MODULE_PATH

    insmod $MODULE_PATH $dxConfig  2>>$LOGFILE
    if [ $? != "0" ] ; then
        echo
        log_failure "ERROR: insmod failed -  see $LOGFILE and syslog"
        exit 1
    fi

    DXmajorNum=`grep $MODULE_BASE /proc/devices | awk '{print $1}'`
    if [ "$DXmajorNum" = "" ]; then
        echo
        log_failure "ERROR: no DX device -  see $LOGFILE and syslog"
        exit 1
    fi

    log_msg Making devices for DX administrator interface, Major = $DXmajorNum     
    #  Remove old stuff if any device nodes 
    if [ -c $DXDEV/1 ]; then
	   rm -f $DXDEV/*
    fi
    mkdir -p $DXDEV
    chmod 777 $DXDEV
    i=0
    while [ $i -lt 16 ]
       do
       if [ ! -c $DXDEV/$i ]; then
            mknod $DXDEV/$i c $DXmajorNum $i
            chmod 666 $DXDEV/$i
       fi
       i=`expr $i + 1`
    done
    sync

    # after successful start create lockfile (otherwise redhat won't execute stop)
    if [ -d /var/lock/subsys ] ; then
        touch /var/lock/subsys/$SERVICE_NAME
    fi
    log_success 
    sync
    ;;

stop)
    show_msg -n Stopping Dolphin DX driver 

    # don't show "failure" if already stopped
    if [ -n "`lsmod | grep $MODULE_BASE`" ] ; then
        rmmod $MODULE_BASE 2>>$LOGFILE
        if [ $? != "0" ] ; then
            echo
            log_failure "ERROR: rmmod failed - see $LOGFILE and syslog"
            exit 1
        fi
    fi

    # Remove device nodes 
    if [ -c $DXDEV/1 ]; then
	   rm -f $DXDEV/*
    fi

    # after successful stop remove lockfile
    if [ -d /var/lock/subsys ] ; then
        rm -f /var/lock/subsys/$SERVICE_NAME
    fi
    log_success 
    ;;
    
restart)
    sh $0 stop
    sh $0 start    
    ;;
    
status)
    if [ -n "`lsmod | grep $MODULE_BASE`" ] ; then
        DXmajorNum=`grep $MODULE_BASE /proc/devices | awk '{print $1}'`
        if [ -n "$DXmajorNum" -a -c $DXDEV/1 ]; then
            # DX is running for sure
            # Actually, we can not be sure that the module is from the executable we check above!
            # Another way to determine the version?
            echo "$MODULE_VERSION is running."
            exit 0
        else
            log_warning "Dolphin DX module loaded, but no device links found."
            exit 4
        fi
    else    
        if [ -r /var/lock/subsys/$SERVICE_NAME ] ; then
            log_warning "Dolphin DX not running, but /var/lock/subsys/$SERVICE_NAME exists"
            exit 2
        else
            echo "Dolphin DX driver is stopped."
            exit 3
        fi
    fi
    ;;
    
*)
    echo "Usage: $0 {start | stop | restart | status}"
    exit 1
    ;;
esac

