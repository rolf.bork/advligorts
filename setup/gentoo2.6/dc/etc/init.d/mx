#!/bin/sh

# chkconfig: 2345 30 70
# description: starts MX driver for Myrinet card

### BEGIN INIT INFO
# Provides:          mx
# Required-Start:    $network $syslog $remote_fs
# Required-Stop:     $network $syslog $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       Starts the MX driver
### END INIT INFO

# add/modify/uncomment MX_MODULES_PARAMS lines to change defaults
#MX_MODULE_PARAMS=" mx_max_endpoints=16 $MX_MODULE_PARAMS"
#MX_MODULE_PARAMS=" mx_max_nodes=64 $MX_MODULE_PARAMS"
MX_MODULE_PARAMS="mx_max_instance=2 mx_max_endpoints=16 $MX_MODULE_PARAMS"

#Note this is replaced by make install, not configure!
export MX_MODULE_DIR=/opt/mx/sbin
PATH=/usr/bin:/bin:/usr/sbin:/sbin

set -e

if test ! -d ${MX_MODULE_DIR}; then
    echo "Something bad happened with the install script"
    echo "MX_DIR isn't pointing to a valid directory"
    exit 1
fi

# Stop the Ethernet-over-Myrinet device(s)
stop_myri_ether() {
    /sbin/ifconfig -a | grep "^myri[0-9]" | awk '{ print $1 }' | cut -d: -f1 | uniq |
    while read dev
    do
	/sbin/ifconfig ${dev} down 2>&1 > /dev/null
    done
}

# Remove the MX modules if loaded
unload_mx() {
    stop_myri_ether
    ${MX_MODULE_DIR}/mx_stop_mapper
    if grep myri_mx /proc/modules > /dev/null; then
	echo "Removing myri_mx driver"
	/sbin/rmmod myri_mx
    fi
    if grep mx_driver /proc/modules > /dev/null; then
	echo "Removing mx driver"
	/sbin/rmmod mx_driver
    fi
	
    if grep mx_mcp /proc/modules > /dev/null; then
	echo "Removing mx mcp"
	/sbin/rmmod mx_mcp
    fi
}

# Remove the GM module if loaded
unload_other() {
    if grep gm /proc/modules > /dev/null; then
	if test -x /etc/init.d/gm; then
	    echo "Unloading gm driver"
	    /etc/init.d/gm stop
	elif grep -q "^220 gm" /proc/devices ; then
	    echo "Unloading gm driver"
	    pkill gm_mapper && sleep 1
	    /sbin/rmmod gm
        fi
    fi
    if grep -q myri10ge /proc/modules ; then
	/sbin/rmmod myri10ge
    fi
    if test -d /sys/bus/pci/drivers/myri10ge &&
       ls /sys/bus/pci/drivers/myri10ge/ | fgrep -q 00.0 ; then
      (cd /sys/bus/pci/drivers/myri10ge &&
       for dev in *:00.0 ; do echo -n $dev > unbind;done )
    fi
    awk '/^(myri)/ { print $1 }' /proc/modules |
     while read module; do
	echo "Removing $module"
	/sbin/rmmod $module
     done
}

activate_page_syms () 
{
    kver=`uname -r`
    if test -r /boot/System.map-$kver ; then 
	a="0x0`grep  ' [DT] activate_page$' /boot/System.map-$kver | sed -e 's/ .*//'`"
	s="0x0`grep  ' [DT] sprintf$' /boot/System.map-$kver | sed -e 's/ .*//'`"
	if test -n "$a" && test -n "$s" ;  then
	    params="$params mx_activate_page_symbol=$a mx_sprintf_symbol=$s "
	fi
    fi
}

# Load the MX modules
load_mx() {
    if grep "swiotlb=[^ ]*force" /proc/cmdline ; then
      echo "ERROR: MX is incompatible with software iotlb (swiotlb=force found in /proc/cmdline)" >&2
      exit 1
    fi
    echo "Loading mx driver"
    params=" mx_mapper_path=${MX_MODULE_DIR}/mx_start_mapper $MX_MODULE_PARAMS $*"

    case `uname -r` in 
     2.6.*|3.*)
	/sbin/insmod ${MX_MODULE_DIR}/mx_mcp.ko || :
	# the kernel zlib is only used on 2.6 (>= 2.6.10)
	if /sbin/modinfo ${MX_MODULE_DIR}/mx_driver.ko | grep ^depends: | grep zlib_inflate > /dev/null 2>&1 ; then
          modprobe zlib_inflate
	fi
	# LRO is used on >= 2.6.24
	if /sbin/modinfo ${MX_MODULE_DIR}/mx_driver.ko | grep ^depends: | grep inet_lro > /dev/null 2>&1 ; then 
	  modprobe inet_lro
	fi
	/sbin/insmod ${MX_MODULE_DIR}/mx_driver.ko $params
        ;;
     2.4.*)
        activate_page_syms
	/sbin/insmod ${MX_MODULE_DIR}/mx_mcp.o
	/sbin/insmod ${MX_MODULE_DIR}/mx_driver.o $params
       ;;
     *)
       echo "ERROR: Don't know about kernel version `uname -r`" >&2
       exit 1
      ;;
    esac
    sleep 1
    echo "Creating mx devices"
    $MX_MODULE_DIR/mx_create_devs
    #${MX_MODULE_DIR}/mx_start_mapper
    return 0

}

cmd="$1"

shift

case "$cmd" in
    start|"")
        if ! echo "$MX_MODULE_PARAMS $*" | grep -q "mx_bus=\|mx_mac=" ; then
	  unload_other
        fi
        if $MX_MODULE_DIR/../bin/mx_endpoint_info 2> /dev/null | grep raw > /dev/null ; then
          :
        else
          unload_mx
        fi
	load_mx "$@"
	;;
    stop)
	unload_mx
	;;
    start-mapper)
	${MX_MODULE_DIR}/mx_start_mapper
	;;
    stop-mapper)
	${MX_MODULE_DIR}/mx_stop_mapper
	;;
    status)
	if grep mx_driver /proc/modules > /dev/null; then
	    echo "MX driver is loaded"
	else
	    echo "MX driver is not loaded"
	fi
	;;
    restart)
        if ! echo "$MX_MODULE_PARAMS $*" | egrep -q "mx_bus=\|mx_mac=" ; then
	  unload_other
        fi
	unload_mx
	load_mx "$@"
	;;
    *)
	echo $"Usage: $0 {start|stop|start-mapper|stop-mapper|status|restart}"
        exit 1
esac

exit 0
