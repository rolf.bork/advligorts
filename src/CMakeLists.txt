add_subdirectory(gds)
add_subdirectory(daqd)
add_subdirectory(dv)
add_subdirectory(nds)
add_subdirectory(mx_stream)
add_subdirectory(run_number)
add_subdirectory(zmq_stream)
