NOTICE  ******
	*** None of the software in this directory is presently suitable for use in a production system.
	*** Code here is intended for prototype testing of 0MX network protocol in FE -> DAQ LAN.

Files:
- zmq_multi_stream.c:
	- Purpose: Send DAQ data for all models running on a single FE computer via the DAQ LAN.  
	- Usage: zmq_multi_stream -s systems -e ethernet interface, where:
		- ethernet interface = ethernet device to send data on eg eth1
		- systems (optional) are space delimited names of all models running on the FE computer 
		  eg -s "x2ioplsc0 x2lsc x2omc x2lscaux"
		  - NOTE: **** If -s is not specified, code will use testpoint.par and hostname
		    to determine which control models to attach to.
		    *******
	- Status: Works fine if all models running on the FE
		- Recently added some timing checks ie is model running, all on same timestamp, etc..

- zmq_threads.c:
	- Purpose: DAQ data concentrator. Code uses a thread to connect to each FE computer DAQ net.
	- Usage: zmq_threads -s computer_names, where:
		- computer_names = hostnames corresponding to FE computer DAQ LAN connection.
		  e.g. -s "x2lsc0_daq x2oaf0_daq"
	- Status: Functional to point of being able to rcv data, place into larger DC block and
		retransmit on the DAQ 10G LAN. By default, publishes on eth2, but should be argument
		to program.

- zmq_rcv_from_dc.c:
	- Purpose: Receive DAQ data from data concentrator (zmq_threads).
	- Usage: zmq_rcv_from_dc -s server, where:
		- server is name of data concentrator on DAQ 10G network.
	- Status: Functional to point of being able to rcv data from DC, but doesn't do anything
		with the data as yet.

- zmq_multi_rcvr:
	- Purpose: Single threaded version of zmq_threads.c
	- Status: Not functional, as work continued with zmq_threads instead.

- zmq_proxy.c
	- Purpose: Receive data from DAQ LAN and retranmit data in channel by channel blocks.
	- Usage: zmq_proxy -s feserver -d daq_file_name(s), where:
		- feserver = name of front end computer on FE-DAQ LAN, eg -s x2lsc0_daq.
		- daq_file_name = one or multiple (in "") fe computer DAQ file names.
		- EXAMPLE:
			zmq_proxy -s x2lsc0_daq -d "x2ioplsc0 x2lsc x2omc x2lscaux"
		- NOTE: list of daq files must be same order as specified to zmq_multi_stream
			running on the FE computer ie code is not yet smart enough to use
			the dcu ids being sent by the FE computers.
	- Status: Limited functionality:
		- Hard coded to send data on x2daqdc0, eth2 (DAQ 10G) 
		- Can only receive data from 1 FE computer.
		- Cannot receive data from data concentrator.

- zmq_proxy_client.c
	- Purpose: Subscribe to live data streams of single or multiple DAQ channels by name
		   from the zmq_proxy.
	- Usage: zmq_proxy_client channame, where:
		- channame = one or more valid DAQ channel names.
		- Examples:
		  - Get data from a single channel:
			zmq_proxy_client  X2:LSC-ODC_CHANNEL_LATCH 
		  - Get data from multiple channels using descrete names:
		  	zmq_proxy_client  X2:LSC-ODC_CHANNEL_LATCH  X2:LSC-ODC_CHANNEL_LATCH 
		  - Get data from multiple channels using wildcard:
			zmq_proxy_client  X2:LSC-ODC_CHANNEL_   (connects to every channel that
								begins with the string provided)
	- Status: Functional, but limited to use on x2daqnds0 at present. 

- zme_pc.py
	- Purpose: Receive status information from zmq_threads and send to EPICS channels defined in 
	 	   dc.db.
	- Status: Functional, but hard coded for use on Caltech test system.

- gdstp_test.c
	- Purpose: Test delivery of GDS TP data by channel from real-time daqLib.c code.
	- Usage: gdstp_test -s "modelnames" where:
		- modelnames = name of control models running on this front end computer
		- NOTE: Code must run on same computer as the real-time front end control applications
	- Output: If a GDS TP is selected on any one of the models listed in the -s argument, the code will
		print out data at 16Hz:
			- Model name and cycle number (0-15)
			- Channel Name
			- First 5 data values of the data stream 
		EXAMPLE *********
		x1iopsam - New cycle found 5 with 1 GDS chans
		channel name = X1:IOP-SAM_ADC_DUOTONE_IN1
		data 0 = 1836.000000
		data 1 = 1952.000000
		data 2 = 2052.000000
		data 3 = 2132.000000
		data 4 = 2198.000000
		x1fe3tim16 - New cycle found 5 with 1 GDS chans
		channel name = X1:FE3-TIM16_T1_INPUT_FILT_TEST1
		data 0 = -69.822632
		data 1 = -55.574661
		data 2 = -33.132141
		data 3 = -6.907826
		data 4 = 20.496559

