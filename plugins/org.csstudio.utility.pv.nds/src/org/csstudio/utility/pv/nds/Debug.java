package org.csstudio.utility.pv.nds;

/**
 * Controls volume of debugging messages
 */
public interface Debug {
  static final int _debug = 1;
  static final boolean DIALOGS_ARE_MODAL = false;
  static final int VERSION = 1;
  static final int REVISION = 6;
}
