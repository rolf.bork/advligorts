package org.csstudio.utility.pv.nds;

public interface Heartbeat {
  public abstract boolean isDead (int blocksLeft);
}
