package org.csstudio.utility.pv.nds;

/**
 * Preferences defaults
 */
public interface Defaults {
  static final String defaultPort = "localhost:8088";
  static final String defaultFramerHost = "localhost";
  static final int defaultFramerPort = 8088;
  static final String defaultImageMapUrl = "http://www.ligo.caltech.edu/~aivanov/image_map/";
  static final String defaultFileDirectory = "";
}
