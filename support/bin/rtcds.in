#!/bin/bash -e

RTS_VERSION=${RTS_VERSION:-__VERSION__}
ENV_FILE=${RTS_ENV:-/etc/advligorts/env}
RCG_SRC=${RCG_SRC:-/usr/share/advligorts/src}
RCG_BUILDD=${RCG_BUILDD:-/var/cache/advligorts/rcg-$RTS_VERSION}
RTS_USER=${RTS_USER:-controls}
RTSYSTAB=/etc/rtsystab

source "$ENV_FILE" 2>/dev/null || true

SITE=${SITE^^*}
site=${SITE,,*}
IFO=${IFO^^*}
ifo=${ifo,,*}

# search paths for C source code
CDS_SRC=${CDS_SRC:-$RCG_LIB_PATH}
CDS_IFO_SRC=${CDS_IFO_SRC:-$CDS_SRC}

# add RCG module source to lib path
RCG_LIB_PATH=$RCG_LIB_PATH:${RCG_SRC}/src/epics/simLink/:${RCG_SRC}/src/epics/simLink/lib

USER_VARS=(SITE IFO OPTRTCDS RCG_LIB_PATH)
LIST_VARS=(RTS_VERSION ${USER_VARS[@]} RCG_SRC RCG_BUILDD RTS_USER RTSYSTAB)
EXPORT_VARS=(${USER_VARS[@]} site ifo CDS_SRC CDS_IFO_SRC)

##################################################

log() {
    echo "$@" >&2
}

check_env() {
    for var in ${USER_VARS[*]} ; do
	if [ ! "${!var}" ] ; then
	    log "ERROR: variable '$var' not set."
	    log "The following environment variables must be set (in e.g. $ENV_FILE):"
	    for vv in ${USER_VARS[*]} ; do
		log "   $vv"
	    done
	    exit 1
	fi
    done
    for var in ${LIST_VARS[*]} ; do
	log "$var=${!var}"
    done
    for var in ${EXPORT_VARS[*]} ; do
	export $var
    done
}

# FIXME: the RCG currently requires that the kernel source be at
# /usr/src/linux.  this checks that /usr/src/linux is a symlink to the
# header dir for the currently running kernel.  This shouldn't be
# necessary once the RCG is fixed.
check_linux_src() {
    if [ "$RCG_IGNORE_KERNEL_RELEASE" ] ; then
	return
    fi
    local target=$(readlink /usr/src/linux)
    local msg=
    if [ ! -d /usr/src/linux ] ; then
	msg="Missing linux source."
    elif [ -z "$target" ] ; then
	msg="Unknown linux source."
    fi
    local release=$(uname -r | sed 's/-amd64$/-common/')
    for ls in /usr/src/linux-headers-${release}{,-common} ; do
	if [[ "$target" == "$ls" ]] ; then
	    return
	fi
	if [ -d "$ls" ] ; then
	    break
	fi
    done
    if [ -z "$msg" ] ; then
	msg="Linux source does not match currently running kernel."
    fi
    log "$msg

The RCG expects the linux source to be at /usr/src/linux.  For modern
distros this should be a symlink to the source installed as part of
the kernel header package for the running kernel.  For this system:

/usr/src/linux -> $ls

Please create/update this link, or set the RCG_IGNORE_KERNEL_RELEASE
environment variable to bypass this check."

    exit 10
}

prep_target(){
    log "creating OPTRTCDS..."
    mkdir -p ${OPTRTCDS}/{target,chans}/tmp
}

prep_buildd() {
    sudo mkdir -p "$RCG_BUILDD"
    sudo chown "$RTS_USER" "$RCG_BUILDD"
    if [ -e "$RCG_BUILDD"/Makefile ] ; then
	return
    fi
    log "configuring RCG_BUILDD..."
    cd "$RCG_BUILDD"
    "$RCG_SRC"/configure
}

########

list_host_sys() {
    local host=$(hostname -s)
    local systems=($(grep -s "^${host}" $RTSYSTAB | sed s/$host//))
    echo ${systems[@]}
}

tname() {
    if [[ ${1,,*} == 'iop' ]] ; then
	list_host_sys | head -1
    else
	echo "$1"
    fi
}

check_host_fe() {
    if [ -z "$(list_host_sys)" ] ; then
	log "No $RTSYSTAB defined for this system."
	exit 3
    fi
}

check_host_sys() {
    for sys ; do
	for hsys in $(list_host_sys) ; do
	    [[ "$hsys" == "$sys" ]] && return 0
	done
	log "Cannot start/stop system '$sys' on host $(hostname -s)."
	exit 4
    done
}

########

build_sys() {
    cd $RCG_BUILDD
    for sys ; do
	sys=$(tname "$sys")
	log "### building $sys..."
	make $sys
    done
}

install_sys() {
    cd $RCG_BUILDD
    for sys ; do
	sys=$(tname "$sys")
	log "### installing $sys..."
	make install-$sys
    done
}

start_sys() {
    for sys ; do
	sys=$(tname "$sys")
	log "### starting $sys..."
	sudo systemctl start rts@${sys}.target
    done
}

stop_sys() {
    for sys ; do
	sys=$(tname "$sys")
	log "### stopping $sys..."
	#${SCRIPTD}/kill${sys}
	sudo systemctl stop rts@${sys}.target
    done
}

enable_sys() {
    for sys ; do
	sys=$(tname "$sys")
	log "### enabling $sys..."
	sudo systemctl enable rts@${sys}.target
    done
}

disable_sys() {
    for sys ; do
	sys=$(tname "$sys")
	log "### disabling $sys..."
	sudo systemctl disable rts@${sys}.target
    done
}

_lsmod() {
    local systems=($(list_host_sys))
    local rts=(mbuf symmetricom)
    local omx=(open_mx)
    local dis=(dis_kosif dis_dx dis_irm)

    # FIXME: how to test for needed modules?
    modules=(${rts[*]} ${omx[*]} ${dis[*]})
    
    local allloaded=
    for m in ${modules[*]}; do
	md=$(lsmod | grep "^${m}") || true
	if [ -z "$md" ] ; then
	    printf "%-18s ***NOT LOADED***\n" "$m"
	    allloaded=1
	else
	    echo "$md"
	fi
    done
    if [ "$systems" ] ; then
	echo
	for m in ${systems[*]}; do
	    md=$(lsmod | grep "^${m}") || true
	    if [ -z "$md" ] ; then
		printf "%-18s ***NOT LOADED***\n" "$m"
		allloaded=1
	    else
		echo "$md"
	    fi
	done
    fi
    if [ "$allloaded" ] ; then
	return 4
    fi
}

############################################################

usage() {
    echo "Usage: $(basename $0) <command> [args]

Advanced LIGO Real Time System control interface.

Available commands:

  build|make <sys>           build system
  install <sys>              install system

  start <sys>...|--all       start systems
  restart <sys>...|--all     restart running systems
  stop|kill <sys>...|--all   stop running systems
  enable <sys>...|--all      enable system start at boot
  disable <sys>...|--all     disable system start at boot

  blog <sys>                 show last build log for system
    -i                         print log paths
  list|ls                    list all systems for current host
  lsmod                      list loaded RTS kernel modules
  dtail|dmesg                tail dmesg logs
  env                        print system environment info
  version|--version|-v       print version
  help|--help|-h             this help
"
}

if [ "$1" ] ; then
    cmd=$1
    shift
else
    log "You must specify a command."
    log
    usage
    exit 1
fi

case $cmd in
    'build'|'make')
	if [ -z "$1" ] ; then
	    log "You must specify at least one system to build."
	    exit 2
	fi
	check_linux_src
	check_env
	prep_buildd
	build_sys $@
	;;
    'install')
	if [ -z "$1" ] ; then
	    log "You must specify at least one system to install."
	    exit 2
	fi
	check_env
	install_sys $@
	;;
    'start')
	if [ -z "$1" ] ; then
	    log "You must specify at least one system to start (or '--all')."
	    exit 2
	fi
	check_host_fe
	if [[ "$1" == '--all' ]] ; then
	    start_sys $(list_host_sys)
	else
	    check_host_sys $@
	    start_sys $@
	fi
	;;
    'restart')
	if [ -z "$1" ] ; then
	    log "You must specify at least one system to restart (or '--all')."
	    exit 2
	fi
	check_host_fe
	if [[ "$1" == '--all' ]] ; then
	    # we do this in reverse so the IOP is stopped last
	    stop_sys $(list_host_sys | tac)
	    start_sys $(list_host_sys)
	else
	    check_host_sys $@
	    stop_sys $@
	    start_sys $@
	fi
	;;
    'stop'|'kill')
	if [ -z "$1" ] ; then
	    log "You must specify at least one system to stop (or '--all')."
	    exit 2
	fi
	check_host_fe
	if [[ "$1" == '--all' ]] ; then
	    # we do this in reverse so the IOP is stopped last
	    stop_sys $(list_host_sys | tac)
	else
	    check_host_sys $@
	    stop_sys $@
	fi
	;;
    'enable'|'disable')
	if [ -z "$1" ] ; then
	    log "You must specify at least one system to $cmd (or '--all')."
	    exit 2
	fi
	check_host_fe
	if [[ "$1" == '--all' ]] ; then
	    # we do this in reverse so the IOP is stopped last
	    ${cmd}_sys $(list_host_sys | tac)
	else
	    check_host_sys $@
	    ${cmd}_sys $@
	fi
	;;
    'blog')
	format=full
	if [[ "$1" == '-i' ]] ; then
	    format=info
	    shift
	fi
	if [ -z "$1" ] ; then
	    log "You must specify at least one system to view."
	    exit 2
	fi
	olog="${RCG_BUILDD}/${1}.log"
	elog="${RCG_BUILDD}/${1}_error.log"
	echo $olog
	case "$format" in
	    info)
		ls -al "$olog" "$elog"
		;;
	    full)
		sed 's/^/stdout:/' < "$olog"
		sed 's/^/stderr:/' < "$elog"
		;;
	esac
	;;
    'list'|'ls')
	check_host_fe
	list_host_sys | tr ' ' '\n'
	;;
    'lsmod'|'lsmods')
	_lsmod
	;;
    'dtail'|'dmesg')
	LINES=$(($(tput lines) - 14))
	watch -n1 "dmesg | tail -$LINES"
	;;
    'env')
	check_env
	;;
    'version'|'v'|'vers'|'-v'|'--version')
	echo $RTS_VERSION
	;;
    'help'|'-h'|'--help')
	usage
	;;
    *)
	log "Unknown command: $cmd"
	log
	usage
	exit 1
	;;
esac
